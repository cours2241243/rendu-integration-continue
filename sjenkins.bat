set JENKINS_HOME=C:\Projets\Java\Jenkins\runtime\workspace
set JENKINS_PORT=9090
set JENKINS_CONTEXT=/jenkins

java -jar jenkins.war --httpPort=%JENKINS_PORT% --httpListenAddress=127.0.0.1 --prefix=%JENKINS_CONTEXT%
