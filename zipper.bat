@echo off
setlocal

REM Spécifiez le chemin du dossier que vous souhaitez compresser
set dossier_source=C:\Projets\Python\integration-continue

REM Spécifiez le chemin et le nom du fichier zip de sortie
set fichier_zip=C:\Users\valen\Downloads\production

REM Spécifiez le chemin vers l'exécutable 7zip
set 7zip=C:\Program Files\7-Zip\7z.exe

REM Compression avec 7zip
%7zip% a -tzip %fichier_zip% %dossier_source%

echo Compression terminée.
pause
